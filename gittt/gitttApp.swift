//
//  gitttApp.swift
//  gittt
//
//  Created by Giusy Di Paola on 15/11/21.
//

import SwiftUI

@main
struct gitttApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
